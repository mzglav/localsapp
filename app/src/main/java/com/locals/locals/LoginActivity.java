package com.locals.locals;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import java.util.Objects;


public class LoginActivity extends AppCompatActivity {
    private EditText etUserName, etPassword;
    Preferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.etUserName = (EditText) findViewById(R.id.etUserName);
        this.etPassword = (EditText) findViewById(R.id.etPassword);
        mPrefs = new Preferences(getApplicationContext());
    }


    public void loginClick(View view) {
            User mUser = mPrefs.getUser(etUserName.getText().toString());
            if (mUser != null &&
                    !(etUserName.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()))
                if (Objects.equals(mUser.getPassword(), etPassword.getText().toString())) {
                    Intent mIntent = new Intent(LoginActivity.this, MainWindow.class);
                    LoginActivity.this.startActivity(mIntent);
               }
                else Toast.makeText(getApplicationContext(), "Wrong password!", Toast.LENGTH_LONG).show();
            else Toast.makeText(getApplicationContext(), "Wrong username!", Toast.LENGTH_LONG).show();
    }

    public void signUpClick(View view) {
        Intent signUpIntent = new Intent(LoginActivity.this, SignUpActivity.class);
        LoginActivity.this.startActivity(signUpIntent);
    }
}