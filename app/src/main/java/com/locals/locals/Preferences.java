package com.locals.locals;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

public class Preferences {
    String PREFS_NAME = "MyPreferences";
    SharedPreferences sharedPreferencesInstance;

    public Preferences(Context context) {
        sharedPreferencesInstance = context.getSharedPreferences(PREFS_NAME, 0);
    }

    public void putUser(String key, User mUser) {
        SharedPreferences.Editor prefsEditor = sharedPreferencesInstance.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mUser);
        prefsEditor.putString(key, json);
        prefsEditor.commit();
    }

    public User getUser(String key) {
        Gson gson = new Gson();
        String json = sharedPreferencesInstance.getString(key, "");
        User user = gson.fromJson(json, User.class);
        return user;
    }

}
