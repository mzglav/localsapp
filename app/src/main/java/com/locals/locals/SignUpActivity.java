package com.locals.locals;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;


public class SignUpActivity extends AppCompatActivity {

    private EditText etName, etSurname, etEmail, etPhone, etUsername, etPassword;
    Preferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mPrefs = new Preferences(getApplicationContext());
        etName = (EditText) findViewById(R.id.etName);
        etSurname = (EditText) findViewById(R.id.etSurname);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public void signUpClick(View view) {
        if (!(etName.getText().toString().isEmpty() ||
                etSurname.getText().toString().isEmpty() ||
                etUsername.getText().toString().isEmpty() ||
                etEmail.getText().toString().isEmpty() ||
                etPhone.getText().toString().isEmpty() ||
                etPassword.getText().toString().isEmpty())) {
            User mUser = new User(etName.getText().toString(),
                    etSurname.getText().toString(),
                    etUsername.getText().toString(),
                    etEmail.getText().toString(),
                    etPhone.getText().toString(),
                    etPassword.getText().toString());
            mPrefs.putUser(mUser.getUsername(), mUser);
            Intent signUpIntent = new Intent(SignUpActivity.this, MainWindow.class);
            SignUpActivity.this.startActivity(signUpIntent);
        } else
            Toast.makeText(getApplicationContext(), "You have empty fields!", Toast.LENGTH_LONG).show();
    }

}

