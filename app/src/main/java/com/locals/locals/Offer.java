package com.locals.locals;


import java.io.Serializable;
import java.util.ArrayList;

public class Offer implements Serializable {
    private String name, description, detailedDesc, time, imageURL;
    private String[] categories;
    private double price;
    private User user;

    public Offer(String name, String description, String detailedDesc,
                 String[] categories, double price, String time, String imageURL, User user) {
        this.name = name;
        this.description = description;
        this.detailedDesc = detailedDesc;
        this.categories = categories;
        this.price = price;
        this.time = time;
        this.imageURL = imageURL;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDetailedDesc() {
        return detailedDesc;
    }

    public double getPrice() {
        return price;
    }

    public String getFormatedPrice() {
        String price = String.valueOf(this.price);
        return price.concat(" kn");
    }

    public String getTime() {
        return time;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String[] getCategories() {
        return categories;
    }

    public User getUser() {
        return user;
    }

    public static ArrayList<Offer> populateOffers() {
        ArrayList<Offer> allOffers = new ArrayList<>();
        allOffers.add(new Offer("Izlet na Matokit", "Za entuzijaste  koji vole planinarenje, druženje i zdrav život",
                "To je najjužniji gorski greben Vrgorskog gorja, sjeverozapadno od Vrgorca dužine oko 8,5 km.\n" +
                        "Pruža se smjerom sjeverozapad-jugoistok i na zapadu je usporedan s višim primorskim lancem Biokova, od kojega je odvojen dolinom Ravča - Rašćane.\n" +
                        "Najviši vrh je Sveti Rok (1062 m) s ostacima kapelice sv. Roka, koja je sagrađena na još starijim ostacima Ilirske obredne gomile.\n",
                new String[]{"Trips", "Sport", "Entertainment"}, 120, "18.2.2018.", "http://www.hps.hr/files/data/26/vrhovi.folder/19-10.jpg",
                new User("Ante", "Antic", "aAntic", "antic@grob.com", "+3853231321", null)));
        allOffers.add(new Offer("Najpoznatija jela svjetskih kuhinja", "Tečaj kuhanja za amatere",
                "Tema su najpoznatija jela američke, belgijske, ruske i gruzijske kuhinje, ali i drugi svjetski specijaliteti.\n" +
                        "Članarina ili kupnja jednog tečaja, savršen je poklon za sve one koji žele i u darivanju biti originalni i pokoniti nezaboravno iskustvo!",
                new String[]{"Cuisine"}, 400, "11.2.2017.", "https://s.zumzi.com/zp/f/6/f6ec5dde59b94f438dda8903ed7edae5_1_large.jpg",
                new User("Bosko", "Boskic", "bBoskic", "bosko@grob.com", "+3853432411", null)));
        allOffers.add(new Offer("Radionica izrade sapuna", "Kvalitetna izrada sapuna, bez korištenja gotovih baza",
                "Na radionici se uči:\n" +
                        "-Sigurno rukovanje kaustičnom sodom\n" +
                        "-kako različita bazna ulja (masne kiseline) utječu na sastav sapuna\n" +
                        "-sastavljanje recepata\n" +
                        "-Praktični dio: izrada sapuna uz objašnjavanje “traga” i oslikavanja sapuna\n" +
                        "-korištenje eteričnih ulja, pigmenata i ostalih dodataka u sapunu\n" +
                        "-najčešće greške pri izradi sapuna i kako ih izbjeći\n" +
                        "-korisni savjeti\n" +
                        "-trajnost i skladištenje hladnih sapuna\n" +
                        "-odgovaranje na pitanja\n!",
                new String[]{"Entertainment"}, 200, "18.5.2016.", "http://www.huped.hr/upload_data/site_photos/domaci_sapuni.jpg",
                new User("Grobko", "Grobic", "gGrobic", "grobko@grob.com", "+385231411", null)));
        allOffers.add(new Offer("Milijun koraka do mora", "Svi idemo od doma na more. Da li ste ikada pomislili pješice otići na more? ",
                "Čak u gradovima uz more, ljudi svakodnevno sjednu u auto, te se voze do omiljene ili najbliže plaže. 750 kilometara preko hrvatskih planina u 30 dana propješačiti SAM je životno iskustvo kojeg se zaboraviti nikada neće! ",
                new String[]{"Trips", "Sport", "Entertainment"}, 100, "1.3.2017.", "https://localz.events/fb/01-2018/predavanje-milijun-koraka-do-mora-cover.jpg",
                new User("Prokleta", "Baza", "pBaza", "proradi@grob.com", "+385232111", null)));
        allOffers.add(new Offer("Sila u kocki", "Bend od srca i duše koji svira kafanski heavy emo! Nas! ",
                "Nekoliko dana nakon Valentinova, svi pravi muški se opijaju. \n" +
                        "Ne moraju više nosit one lipe, fine, lakirane postole, koje vole nosit priko tjedna, nego se raskomotu u tutu suboton navečer i dođu poslušat svoj najdraži bend iz Solina. \n" +
                        "\n ",
                new String[]{"Entertainment"}, 20, "1.3.2231.", "https://www.radiodalmacija.hr/wp-content/uploads/2016/05/sila_necista.jpg",
                new User("Tribaloje", "Pocetprije", "sadJeKasno", "smrc@grob.com", "+3852532411", null)));
        allOffers.add(new Offer("Picigin na Bačvicama", "Atrakcija na Bačvicama, zanimljivo i zabavno. ",
                "U Splitu postoje 4 stvari na koje su njegovi građani ponosni: Dioklecijanova palača pod zaštitom UNESCO-a, park-šuma Marjan " +
                        "(brdo na zapadnoj strani grada, zelena oaza za građane Splita), Hajduk – nogometni klub i picigin, sportska aktivnost ne baš tako poznata u svijetu i istinska posebnost ovoga grada. ",
                new String[]{"Sport", "Entertainment"}, 30, "1.3.1950.", "https://www.hrvatskainfo.com/wp-content/uploads/2009/08/split-ba%c4%8dvice-picigin-1.jpg",
                new User("Prokleti", "Picasso", "zastoNece", "smrc2@grob.com", "+38520982411", null)));
        allOffers.add(new Offer("Sajam pršuta", "Nacionalni sajam pršuta i suhomesnatih proizvoda s međunarodnim sudjelovanjem \n",
                "Sajam ima i revijalni i edukativni karakter pa su se proizvođači i posjetitelji  mogli informirati  o temama vezanim za problematiku proizvodnje i plasmana suhomesnatih " +
                        "proizvoda kao što je dalmatinski pršut, zaštita autohtonih proizvoda i " +
                        "oznaka kvalitete, uvođenje standarda, modeli udruživanja, tržištu, trendovima, mogućnostima kooperacije sa uzgajivačima svinja, tehnologiji proizvodnje i obrade pršuta," +
                        "te programima poticaja i financiranja od strane vladinih  i drugih institucija itd. ",
                new String[]{"Cuisine"}, 10, "1.3.1911.", "http://www.visitsinj.com/Content/images/header/sajam-prsuta.jpg",
                new User("Najgori", "Dizajn", "nDizajn", "bot@grob.com", "+38520321311", null)));
        allOffers.add(new Offer("Turnir u balotama", "Tradicionalna igra u Splitu \n",
                "Boće su izrađene od punog drva ojačanog i učvršćenog čavlima (narodne), plastike (punjene tekućinom ili kojim drugim tvorivom) ili kovine (natjecateljsko igranje)." +
                        "Igra se jedan protiv jednoga, dvoje protiv dvoje, a može se i sa troje protiv troje igrača. ",
                new String[]{"Sport", "Entertainment"}, 10, "5.3.2005.", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Bocce_-_men%27s_game.jpg/1280px-Bocce_-_men%27s_game.jpg",
                new User("Koliko", "Jos", "glupePonude", "kjos@grob.com", "+3852643311", null)));
        allOffers.add(new Offer("MasterClass Split", "Zašto žene vole šminku? \n",
                "Tečaj se sastoji od praktičnog i teorijskog dijela. Polaznice imaju priliku naučiti i usavršiti  najbolje taktike šminkanja. ",
                new String[]{"Entertainment"}, 500, "1.3.1324.", "https://www.kupon.rs/upload/kuponi/5368/thumbs/vikend-skola-sminkanja-popusti-4.jpg",
                new User("Glupa", "Sminka", "samoNacural", "herbalife@grob.com", "+3852213311", null)));
        allOffers.add(new Offer("2Cellos ", "Hrvatski instrumentalni duo \n",
                "Sviranje glazbe na violončelima.\n" +
                        "Od ljeta 2011. sudjeluju na turneji Eltona Johna. Nastupili su u velikom broju talk-showa.\n ",
                new String[]{"Entertainment"}, 170, "1.3.2017.", "https://yt3.ggpht.com/a-/AJLlDp0NNCSoAt9jNFtTAo0OAsiNE1KTjYnJuJyOsA=s900-mo-c-c0xffffffff-rj-k-no",
                new User("Ajmeova", "Dvojica", "svirajMejdene", "2pac@grob.com", "+3852986711", null)));
        return allOffers;
    }
}
