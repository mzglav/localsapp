package com.locals.locals;


import java.io.Serializable;

public class User implements Serializable {
    private String name, surname, username, email, phone, password;

    public User(String name, String surname, String username,
                String email, String number, String password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.phone = number;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

}
