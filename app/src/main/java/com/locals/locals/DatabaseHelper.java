package com.locals.locals;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "locals.db";

    private static final String CREATE_USER_DB = "CREATE TABLE IF NOT EXISTS user_table("
            + "username TEXT NOT NULL PRIMARY KEY, "
            + "name TEXT NOT NULL, "
            + "surname TEXT NOT NULL, "
            + "email TEXT NOT NULL, "
            + "phone TEXT NOT NULL, "
            + "password TEXT NOT NULL "
            + ");";
    private static final String CREATE_OFFERS_DB = "CREATE TABLE IF NOT EXISTS offers_table("
            + "offer TEXT NOT NULL PRIMARY KEY, "
            + "description TEXT NOT NULL, "
            + "detailed TEXT NOT NULL, "
            + "categories TEXT NOT NULL, "
            + "price TEXT NOT NULL, "
            + "time TEXT NOT NULL, "
            + "imageurl TEXT NOT NULL, "
            + "username TEXT NOT NULL"
            + ");";

    private static final String PACKAGE_NAME = "package com.locals.locals";
    private final String DATABASE_PATH = "/data/data/" + PACKAGE_NAME
            + "/databases/" + DATABASE_NAME;
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
        Log.d("CONSTRUCTOR", "Constructing");
    }

    @Override
    public void onCreate(SQLiteDatabase mDatabase) {
        Log.d("CREATE", "Creating database");
        mDatabase.execSQL(CREATE_USER_DB);
        mDatabase.execSQL(CREATE_OFFERS_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase mDatabase, int oldVersion, int newVersion) {
        Log.d("UPGRADE", "Upgrading database");

        onCreate(mDatabase);
    }

}