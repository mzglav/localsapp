package com.locals.locals;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;


public class DatabaseInterface {
    private SQLiteDatabase mDatabase;
    private DatabaseHelper dbHelper;

    public DatabaseInterface(Context context) {
        this.dbHelper = new DatabaseHelper(context);
        try {
            this.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getUser(String username, String password) {
        Cursor cursor;
        if(password != null)
        cursor = mDatabase.rawQuery("SELECT * FROM user_table WHERE username = '"
                        + username + "' AND password = '" + password + "'"
                , null);
        else cursor = mDatabase.rawQuery("SELECT * FROM user_table WHERE username = '"
                        + username + "'"
                , null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            User returnUser = new User(cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(0),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5));
            return returnUser;
        }
        return null;
    }

    public boolean userExists(User user){
        if (getUser(user.getUsername(), null) != null)
            return true;
            else return false;
    }

    public boolean addUser(User user){
        if (!userExists(user)){
            ContentValues values = new ContentValues();
            values.put("username",	user.getUsername());
            values.put("name", user.getName());
            values.put("surname", user.getSurname());
            values.put("email", user.getEmail());
            values.put("phone", user.getPhone());
            values.put("password", user.getPassword());
            mDatabase.insert("user_table", null, values);
            Log.d("DB", "Signed");
            return true;
        }
        else return false;
    }

    public ArrayList<Offer> getAllOffers() {
        ArrayList<Offer> allOffers = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM offers_table", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String[] categories = cursor.getString(3).split(",");
            User user = getUser(cursor.getString(7), null);
            Offer offer = new Offer(cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    categories,
                    Double.valueOf(cursor.getString(4)),
                    cursor.getString(5),
                    cursor.getString(6),
                    user
                    );
            allOffers.add(offer);
        }
        return getAllOffers();
    }

    public void populateOffers() {

    }

    public void open() throws SQLException {
        mDatabase = dbHelper.getWritableDatabase();
    }

    public void close() {
        mDatabase.close();
    }
}
