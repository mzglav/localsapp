package com.locals.locals;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


public class DetailedView extends AppCompatActivity {
    private Offer mOffer;
    private User mUser;
    private EditText etReceiver, etSubject, etBody;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        mOffer = (Offer) getIntent().getSerializableExtra("Offer");
        mUser = mOffer.getUser();
        initialize();
    }

    public void initialize(){
        TextView tvHeadline = (TextView)findViewById(R.id.tvHeadline);
        TextView tvDetailed = (TextView)findViewById(R.id.tvDetailed);
        TextView tvName = (TextView)findViewById(R.id.tvName);
        TextView tvSurname = (TextView)findViewById(R.id.tvSurname);
        TextView tvUsername = (TextView)findViewById(R.id.tvUsername);
        TextView tvEmail = (TextView)findViewById(R.id.tvEmail);
        TextView tvPhone = (TextView)findViewById(R.id.tvPhone);
        TextView tvDate = (TextView)findViewById(R.id.tvDate);
        TextView tvPrice = (TextView)findViewById(R.id.tvPrice);
        etReceiver = (EditText) findViewById(R.id.etReceiver);
        etSubject = (EditText) findViewById(R.id.etSubject);
        etBody = (EditText) findViewById(R.id.etBody);
        ImageView ivImage = (ImageView)findViewById(R.id.ivImage);

        tvHeadline.setText(mOffer.getName());
        tvName.setText(mUser.getName());
        tvSurname.setText(mUser.getSurname());
        tvUsername.setText(mUser.getUsername());
        tvEmail.setText(mUser.getEmail());
        tvPhone.setText(mUser.getPhone());
        tvDate.setText(mOffer.getTime());
        tvPrice.setText(mOffer.getFormatedPrice());
        tvDetailed.setText(mOffer.getDetailedDesc());Resources mResources = getApplicationContext().getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 128, mResources.getDisplayMetrics());
        Picasso.with(getApplicationContext())
                .load(mOffer.getImageURL())
                .resize(px, px)
                .into(ivImage);
    }

    public void sendClick(View view){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{etReceiver.getText().toString()});
        intent.putExtra(Intent.EXTRA_SUBJECT, etSubject.getText().toString());
        intent.putExtra(Intent.EXTRA_TEXT   , etBody.getText().toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(DetailedView.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }
}
