package com.locals.locals;


import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OffersAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Offer> mOffers;

    public OffersAdapter(Context context, ArrayList<Offer> offers) {
        this.mContext = context;
        this.mInflater =
                (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mOffers = offers;
    }

    @Override
    public int getCount() {
        return mOffers.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = mInflater.inflate(R.layout.single_list_item, viewGroup, false);

        TextView tvName = (TextView) view.findViewById(R.id.name);
        TextView tvDescription = (TextView) view.findViewById(R.id.description);
        TextView tvPrice = (TextView) view.findViewById(R.id.price);
        TextView tvNameDate = (TextView) view.findViewById(R.id.tvNameDate);
        ImageView mImage = (ImageView) view.findViewById(R.id.image);
        Offer offer = mOffers.get(i);
        tvName.setText(offer.getName());
        tvDescription.setText(offer.getDescription());
        tvPrice.setText(String.valueOf(offer.getFormatedPrice()));
        tvNameDate.setText(offer.getUser().getUsername() + "   Date: " + offer.getTime());
        Resources mResources = mContext.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, mResources.getDisplayMetrics());
        Picasso.with(mContext)
                .load(offer.getImageURL())
                .resize(px, px)
                .into(mImage);
        return view;
    }

}
