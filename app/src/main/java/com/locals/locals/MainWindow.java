package com.locals.locals;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class MainWindow extends AppCompatActivity {

    private ArrayList<Offer> allOffers;
    private ArrayList<Offer> filteredOffers;
    private ListView offersListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize(){
        allOffers = new ArrayList<Offer>();
        allOffers = Offer.populateOffers();
        filteredOffers = allOffers;
        offersListView = (ListView) findViewById(R.id.offersListView);
        final Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        final Spinner sortSpinner = (Spinner) findViewById(R.id.sortSpinner);

        offersListView.setAdapter(new OffersAdapter(getApplicationContext(), allOffers));
        offersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent mIntent = new Intent(MainWindow.this, DetailedView.class);
                mIntent.putExtra("Offer", filteredOffers.get(position));
                MainWindow.this.startActivity(mIntent);
            }
        });
        ArrayAdapter<CharSequence> categoriesAdapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoriesAdapter);
        ArrayAdapter<CharSequence> sortAdapter = ArrayAdapter.createFromResource(this,
                R.array.sort, android.R.layout.simple_spinner_item);
        sortAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(sortAdapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                filterCategories(categorySpinner.getSelectedItem().toString());
                sortList(sortSpinner);
                offersListView.setAdapter(new OffersAdapter(getApplicationContext(),
                        filteredOffers));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                sortList(sortSpinner);
                offersListView.setAdapter(new OffersAdapter(getApplicationContext(), filteredOffers));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void filterCategories(String filter) {
        ArrayList<Offer> filteredOffers = new ArrayList<>();
        if (filter.equals("All"))
            this.filteredOffers = allOffers;
        else{
            for (Offer offer : allOffers)
                if (Arrays.asList(offer.getCategories()).contains(filter))
                    filteredOffers.add(offer);
            this.filteredOffers = filteredOffers;
        }
    }

    public void sortList(Spinner sortSpinner){
        switch (sortSpinner.getSelectedItem().toString()){
            case "Recent":
                Collections.sort(filteredOffers, new DateComparator());
                Collections.reverse(filteredOffers);
                break;
            case "Oldest":
                Collections.sort(filteredOffers, new DateComparator());
                break;
            case "High-low price":
                Collections.sort(filteredOffers, new PriceComparator());
                Collections.reverse(filteredOffers);
                break;
            case "Low-high price":
                Collections.sort(filteredOffers, new PriceComparator());
                break;
        }
    }

    public class PriceComparator implements Comparator<Offer> {
        @Override
        public int compare(Offer o1, Offer o2) {
            if (o1.getPrice() < o2.getPrice()) return -1;
            if (o1.getPrice() > o2.getPrice()) return 1;
            return 0;
        }
    }

   public class DateComparator implements Comparator<Offer> {
        @Override
        public int compare(Offer o1, Offer o2) {
            String [] str1 = o1.getTime().split("\\.");
            String [] str2 = o2.getTime().split("\\.");
            int time1 = Integer.valueOf(str1[0]) + 30 * Integer.valueOf(str1[1]) + 30 * 12 * Integer.valueOf(str1[2]);
            int time2 = Integer.valueOf(str2[0]) + 30 * Integer.valueOf(str2[1]) + 30 * 12 * Integer.valueOf(str2[2]);
            if (time1 < time2) return -1;
            if (time1 > time2) return 1;
            return 0;
        }
    }
}
